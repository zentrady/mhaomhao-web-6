import Vue from 'vue'
import moment from 'moment'

Vue.filter(
  'numberComma',
  value => !value ? '0' : value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
)

Vue.filter(
  'dateTimeTh',
  value => value ? moment(value).locale('th').format('ll') : value
)

Vue.filter(
  'timeTh',
  value => value ? moment(value).format('HH:mm:ss') : value
)
